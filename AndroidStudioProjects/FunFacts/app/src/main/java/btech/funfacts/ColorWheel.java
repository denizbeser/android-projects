package btech.funfacts;

import android.graphics.Color;

import java.util.Random;

/**
 * Created by user on 25.05.2015.
 */
public class ColorWheel {

    //Member variable properties
    public String[] mColors = {
            "#39add1", // light blue
            "#3079ab", // dark blue
            "#c25975", // mauve
            "#e15258", // red
            "#f9845b", // orange
            "#838cc7", // lavender
            "#7d669e", // purple
            "#53bbb4", // aqua
            "#51b46d", // green
            "#e0ab18", // mustard
            "#637a91", // dark gray
            "#f092b0", // pink
            "#b7c0c7"  // light gray
    };
    //methor (abilities, things objects can do)

    public int getColor(){

        String color = "";

        //randomly select a fact
        Random randomGenerator = new Random(); //construct a new number generator
        int randomNumber = randomGenerator.nextInt(mColors.length);

        //Convert random number to fact
        color = mColors[randomNumber];
        int colorAsInt = Color.parseColor(color);

        return colorAsInt;

    }
}
